import axios from 'axios';

const path = '/file';

export default {

  upload(file) {
    return axios.post(path, file)
  }
}
