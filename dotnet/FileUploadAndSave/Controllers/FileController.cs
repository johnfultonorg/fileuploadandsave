﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileUploadAndSave.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        public async Task<IActionResult> UploadFile([FromForm] ColumnMapper upload)
        {
            if (upload.File != null)
            {
                string uploadPath = "c:\\nicePlace";
                string filename = upload.File.FileName;
                if (upload.File.Length > 0)
                {
                    string filePath = Path.Combine(uploadPath, filename);
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await upload.File.CopyToAsync(fileStream);
                    }
                }
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }

    public class ColumnMapper
    {
        public IFormFile File { get; set; }
    }
}
